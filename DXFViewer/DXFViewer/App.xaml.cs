﻿using System;
using Xamarin.Forms;
using DXFViewer.Views;
using Xamarin.Forms.Xaml;

namespace DXFViewer
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
